set encoding=UTF-8

" indent new lines
set autoindent

" automatically write files when changing when multiple files open
set autowrite

" line numbers
set number

" col row position
set ruler

" active mode
set showmode

set tabstop=2
set softtabstop=2
set shiftwidth=2
set smartindent
set smarttab

" PEP8 settings for python
au BufNewFile,BufRead *.py
    \ set expandtab       " replace tabs with spaces
    \ set autoindent      " copy indent when starting a new line
    \ set tabstop=4
    \ set softtabstop=4
    \ set shiftwidth=4

" stop vim from silently fucking with files that it shouldn't 
set nofixendofline

" replace tabs with spaces automatically
set expandtab

" enough for line numbers + gutter within 80 standard
"set textwidth=80 

" more risky, but cleaner
set nobackup
set noswapfile
set nowritebackup

syntax enable

" faster scrolling
set ttyfast

" allow sensing the filetype
filetype plugin on

" Install vim-plug if not already installed
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
endif
" only load plugins if Plug detected
if filereadable(expand("~/.vim/autoload/plug.vim"))
  set nocompatible

  " load all the plugins
  call plug#begin('~/.vimplugins')
  Plug 'lervag/vimtex' "Plugin for latex
  Plug 'WolfgangMehner/bash-support' " borkish
  Plug 'vim-airline/vim-airline'
  "Plug 'vim-airline/vim-airline-themes'
  "Plug 'airblade/vim-gitgutter'
  Plug 'Townk/vim-autoclose'
  Plug 'sheerun/vim-polyglot'
  Plug 'ryanoasis'
  Plug 'preservim/nerdtree'
  Plug 'davidhalter/jedi-vim'
  Plug 'morhetz/gruvbox'
  Plug 'dracula/vim', { 'as': 'dracula'}
  call plug#end()

  let g:tex_flavor='latex'
  let g:tex_quickfix_mode = 0
  "let g:tex_view_method='zathura'

  "let g:airline_theme = 'base16_gruvbox_dark_medium'
  "colorscheme gruvbox
  colorscheme dracula
  set bg=dark
  "let g:gruvbox_transparent_bg=1 

  let g:jedi#auto_intialization = 1
endif

hi Normal ctermbg=none

" Map alternatives the <ESC> key (<C-[> already is) 
inoremap jj <Esc>
cnoremap jj <Esc>
inoremap kk <Esc> 
cnoremap kk <Esc>
inoremap kj <Esc>
cnoremap kj <Esc> 

" Better page down and page up
noremap <C-n> <C-d>
noremap <C-p> <C-b>
